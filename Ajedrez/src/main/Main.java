package main;

import graphics.ClickAction;
import graphics.Drawing;
import graphics.MyMenuBar;

import javax.swing.JFrame;

public class Main {

	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;

	public static void main(String[] args) throws InterruptedException {	
		JFrame frame = new JFrame("Ajedrez");
		ClickAction clickAction = new ClickAction();
		Drawing drawing = new Drawing();
		frame.add(drawing);
		drawing.addMouseListener(clickAction);
		frame.setSize(WIDTH, HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MyMenuBar myMenuBar = new MyMenuBar();
		myMenuBar.setClickAction(clickAction);
		myMenuBar.setDrawing(drawing);
		myMenuBar.setFrame(frame);
		frame.setJMenuBar(myMenuBar);
		frame.setVisible(true);
	}
}
