package graphics;

import game.Game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;

import javax.swing.JPanel;

import pieces.Piece;
import board.Board;
import javax.swing.JLabel;

public class Drawing extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel label,label2;
	private Game game;
	private Board board;

	
	public Drawing() {
		setLayout(null);
		JLabel label = new JLabel("");
		label.setBounds(470, 100, 330, 20);
		add(label);
		this.label = label;
		JLabel label2 = new JLabel("");
		label2.setBounds(470, 120, 330, 20);
		add(label2);
		this.label2 = label2;
	}
	
	public void update() {
		game.setNeedToUpdate(false);
	}

	public void setGame(Game game) {
		this.game = game;
		this.board = game.getBoard();
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		if (game != null) {
			// Grafico de las celdas
			for (int i = 0; i < Board.getDimension(); i++) {
				for (int j = 0; j < Board.getDimension(); j++) {
					g2d.setColor(board.getSquares()[i][j].getColor());
					if (board.getSquares()[i][j].isMarked())
						g2d.setColor(Color.RED);
					g2d.fillRect(50 + j * 50, 50 + i * 50, 50, 50);
				}
			}

			// Graficos de las piezas
			for (Piece piece : game.getPlayer1().getPieces()) {
				g2d.drawImage(
						Toolkit.getDefaultToolkit().getImage(
								"Images/White" + piece.getName() + ".png"),
						55 + piece.getSquare().getPosition().y * 50, 55 + piece
								.getSquare().getPosition().x * 50, this);
			}
			for (Piece piece : game.getPlayer2().getPieces()) {
				g2d.drawImage(
						Toolkit.getDefaultToolkit().getImage(
								"Images/Black" + piece.getName() + ".png"),
						55 + piece.getSquare().getPosition().y * 50, 55 + piece
								.getSquare().getPosition().x * 50, this);
			}
			g2d.setColor(Color.BLACK);
			g2d.drawRect(50, 50, 400, 400);
			g2d.drawString("Turno de:", 470, 70);
			g2d.setColor(game.getTurn().getColor());
			g2d.fillOval(535, 55, 20, 20);
			g2d.setColor(Color.BLACK);
			g2d.drawOval(535, 55, 20, 20);
		}
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	}

	public void print(String texto) {
		label.setText(texto);
	}
	
	public void print2(String texto) {
		label2.setText(texto);
	}

}
