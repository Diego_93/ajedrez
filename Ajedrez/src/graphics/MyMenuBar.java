package graphics;

import game.Game;
import game.GameLoader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MyMenuBar extends JMenuBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private ClickAction clickAction;
	private Drawing drawing;
	private Game game;

	@SuppressWarnings("deprecation")
	public MyMenuBar() {
		JMenu menu = new JMenu("Archivo");
		JMenuItem menuItem1 = new JMenuItem();
		menuItem1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				newGameAction();
			}
		});
		menuItem1.setLabel("Nueva Partida");
		menu.add(menuItem1);

		JMenuItem menuItem2 = new JMenuItem();
		menuItem2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loadAction();
			}
		});
		menuItem2.setLabel("Cargar Partida");
		menu.add(menuItem2);

		JMenuItem menuItem3 = new JMenuItem();
		menuItem3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveAction();
			}
		});
		menuItem3.setLabel("Guardar Partida");
		menu.add(menuItem3);

		JMenuItem menuItem4 = new JMenuItem();
		menuItem4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		menuItem4.setLabel("Salir");
		menu.add(menuItem4);
		add(menu);
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public void setClickAction(ClickAction clickAction) {
		this.clickAction = clickAction;
	}

	public void setDrawing(Drawing drawing) {
		this.drawing = drawing;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public void loadAction() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("./Saved Games"));
		fileChooser.showDialog(frame.getContentPane(), "Load File");
		if (fileChooser.getSelectedFile() != null) {
			String file = fileChooser.getSelectedFile().getPath();
			try {
				GameLoader gameLoader = new GameLoader();
				initializateGame(gameLoader.loadGame(file));
				drawing.print("Partida cargada exitosamente");
				drawing.print2("");
			} catch (ClassNotFoundException | IOException e1) {
				drawing.print("No se pudo cargar el archivo");
				drawing.print2("");
			}
		}
	}

	public void saveAction() {
		if (game != null) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File("./Saved Games"));
			fileChooser.showDialog(frame.getContentPane(), "Save File");
			if (fileChooser.getSelectedFile() != null) {
				String file = fileChooser.getSelectedFile().getPath();
				GameLoader gameLoader = new GameLoader();
				try {
					gameLoader.saveGame(game, file);
					drawing.print("Partida guardada exitosamente");
					drawing.print2("");
				} catch (IOException e) {
					drawing.print("No se pudo guardar el archivo");
					drawing.print2("");
				}
			}
		}
	}

	public void newGameAction() {
		Game game = new Game();
		initializateGame(game);
	}
	
	public void initializateGame(Game game)
	{
		game.setDrawing(drawing);
		drawing.setGame(game);
		clickAction.setGame(game);
		drawing.print("");
		drawing.print2("");
		setGame(game);
		game.update();
	}
}
