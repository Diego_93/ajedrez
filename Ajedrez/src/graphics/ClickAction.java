package graphics;

import game.Game;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ClickAction implements MouseListener {

	private Game game;

	@Override
	public void mouseClicked(MouseEvent e) {
		if (game != null) {
			Point position = e.getPoint();
			position.x /= 50;
			position.y /= 50;
			if (position.x > 0 && position.x < 9 && position.y > 0	&& position.y < 9){
				game.actionPoint(new Point(position.y - 1, position.x - 1));
			}
		}
	}

	public void setGame(Game game) {
		this.game = game;
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
