package game;

import graphics.Drawing;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;

import players.Player;
import board.Board;
import board.Square;

public class Game implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Drawing drawing;
	private Board board;
	private Player player1, player2;
	private static final Player NoPlayer = null;
	private Player winner = NoPlayer;
	private Player turn;
	private boolean drawn = false;
	private boolean needToUpdate;
	private HistoryOfPlays historyOfPlays;
	private int amountPiecesLastMove=32;
	private int movesWithOutKillOrMoveAPawn=0;

	public Game() {
		setBoard(new Board(this));
		player1 = new Player("White", Color.WHITE);
		player2 = new Player("Black", Color.BLACK);
		setPlayer1(player1);
		setPlayer2(player2);
		setTurn(player1);
		player1.setOpponent(player2);
		player2.setOpponent(player1);
		board.fillBoard();
		historyOfPlays=new HistoryOfPlays();
		historyOfPlays.addMove(this);
	}

	public Drawing getDrawing() {
		return drawing;
	}

	public void setDrawing(Drawing drawing) {
		this.drawing = drawing;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public void update() {
		drawing.repaint();
	}

	public void actionPoint(Point point) {
		if (isGameFinished()) {
			return;
		}
		actionSquare(board.getSquares()[point.x][point.y]);
	}

	public void actionSquare(Square selectSquare) {

		if (board.getActualSquare() == Board.NoSquare
				&& !selectSquare.isEmpty()
				&& selectSquare.getPiece().getOwner().equals(getTurn())) {
			board.setActualSquare(selectSquare);
			board.getActualSquare().setMarked(true);
			update();
		} else {
			if (board.getActualSquare() != Board.NoSquare) {
				if (board.getActualSquare() == selectSquare) {
					board.getActualSquare().setMarked(false);
					board.setActualSquare(Board.NoSquare);
					update();
				} else {
					if (board.getActualSquare().getPiece().moveTo(selectSquare)) {
						board.getActualSquare().setMarked(false);
						board.setActualSquare(Board.NoSquare);
						changeTurn();
						update();
					}
				}
			}
		}
	}

	public boolean isGameFinished() {
		return isAWinner() || isDrawn();
	}

	public Player getWinner() {
		return winner;
	}

	public void setWinner(Player winner) {
		this.winner = winner;
		print("Jaque Mate");
		print2("Ha ganado el jugador: " + winner);
	}

	public boolean isDrawn() {
		return drawn;
	}

	public void setDrawn(String texto) {
		drawn = true;
		print("El juego ha finalizado en tablas");
		print2(texto);
	}

	public boolean isAWinner() {
		return getWinner() != NoPlayer;
	}

	public void changeTurn() {
		setTurn(getTurn().getOpponent());
		print("");
		if (getTurn().inCheck()) {
			print("Jaque");
		}
		
		/*Verifica si hay jaque mate o rey ahogado*/
		if (!getTurn().canMove()) {
			if (getTurn().inCheck()) {
				setWinner(getTurn().getOpponent());
			} else {
				setDrawn("El rey esta ahogado");
			}
		}

		/*Verifica si con las piezas que quedan puede haber un ganador*/
		if  ((player1.getPieces().size() ==1 ||
			(player1.getPieces().size() == 2 &&	player1.hasAKnight()) ||
			(player1.getPieces().size() == 2 &&	player1.hasABishop()))
		&&	(player2.getPieces().size() ==1 ||
			(player2.getPieces().size() == 2 &&	player2.hasAKnight()) ||
			(player2.getPieces().size() == 2 &&	player2.hasABishop())))	{
			setDrawn("Insuficientes piezas para ganar");
		}
		
		/*Verifica si hay empate por movimientos consecutivos*/
		if (amountOfPieces()==amountPiecesLastMove) {
			movesWithOutKillOrMoveAPawn++;
		}
		else {
			amountPiecesLastMove=amountOfPieces();
			movesWithOutKillOrMoveAPawn=0;
		}
		
		if (drawByConsecutiveMoves()){
			setDrawn("50 movimientos consecutivos sin comer");
		}
		
		/*Verifica si hay empate por repeticiones*/
		if (!historyOfPlays.addMove(this))
		{
			setDrawn("Se ha repetido el tablero 3 veces");
		}
			
	}

	public Player getTurn() {
		return turn;
	}

	public void setTurn(Player turn) {
		this.turn = turn;
	}

	public boolean isNeedToUpdate() {
		return needToUpdate;
	}

	public void setNeedToUpdate(boolean needToUpdate) {
		this.needToUpdate = needToUpdate;
	}

	public void print(String texto)
	{
		getDrawing().print(texto);
	}
	
	public void print2(String texto)
	{
		getDrawing().print2(texto);
	}
	
	public String code()
	{
		return board.code()+getTurn().code();
	}
	
	public int amountOfPieces() {
		return getPlayer1().getPieces().size()+getPlayer2().getPieces().size();
	}
		
	public void moveAPawn(){
		movesWithOutKillOrMoveAPawn=-1;
	}
	
	public boolean drawByConsecutiveMoves(){
		return movesWithOutKillOrMoveAPawn==50;
	}
}
