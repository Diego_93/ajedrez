package game;

import java.io.Serializable;
import java.util.HashMap;

public class HistoryOfPlays implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String,Integer> map;
	
	public HistoryOfPlays()
	{
		map= new HashMap<String,Integer>();
	}
	
	public boolean addMove(Game game)
	{
		String stateOfGameCode=game.code();
		if (map.get(stateOfGameCode)==null) {
			map.put(stateOfGameCode, 1);
			return true;
		}
		else {
			map.put(stateOfGameCode, map.get(stateOfGameCode)+1);
			if (map.get(stateOfGameCode)>2) {
				return false;
			}				
			return true;
		}
	}
	
}
