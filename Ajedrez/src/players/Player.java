package players;

import java.awt.Color;

import java.io.Serializable;
import java.util.LinkedList;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Piece;
import board.Square;

public class Player implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LinkedList<Piece> pieces;
	private String colorString;
	private Color color;
	private Player opponent;
	private King king;
	private int enPassantColumn=-1;

	public Player(String colorString, Color color) {
		this.color = color;
		this.colorString = colorString;
		pieces = new LinkedList<>();
	}

	public void addPiece(Piece piece) {
		this.getPieces().add(piece);
	}

	public void removePiece(Piece piece) {
		this.getPieces().remove(piece);
	}

	public boolean inCheck() {
		return !this.getKing().isSafe();
	}

	public LinkedList<Piece> getPieces() {
		return pieces;
	}

	public void setPieces(LinkedList<Piece> pieces) {
		this.pieces = pieces;
	}

	public String getColorString() {
		return colorString;
	}

	public void setColorString(String colorString) {
		this.colorString = colorString;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Player getOpponent() {
		return opponent;
	}

	public void setOpponent(Player opponent) {
		this.opponent = opponent;
	}

	public King getKing() {
		return king;
	}

	public void setKing(King king) {
		this.king = king;
	}

	public boolean canMove() {
		for (Piece piece : getPieces()) {
			if (piece.canMove()) {
				return true;
			}
		}
		return false;
	}

	public boolean canAttack(Square square) {
		for (Piece piece : getPieces()) {
			if (piece.canAttack(square)) {
				return true;
			}
		}
		return false;
	}
	
	public int getEnPassantColumn() {
		return enPassantColumn;
	}

	public void setEnPassantColumn(int enPassantColumn) {
		this.enPassantColumn = enPassantColumn;
	}

	public String toString()
	{
		return getColorString();
	}

	public String code()
	{
		return ""+colorString.charAt(0);
	}
	
	public boolean hasAKnight()
	{
		for (Piece piece : pieces) {
			if (piece instanceof Knight){
				return true;
			}
		}
		return false;
	}
	
	public boolean hasABishop()
	{
		for (Piece piece : pieces) {
			if (piece instanceof Bishop){
				return true;
			}
		}
		return false;
	}
	
}
