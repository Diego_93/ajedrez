package board;

import pieces.*;

import java.awt.*;
import java.io.Serializable;

public class Square  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Board board;
	private Color color;
	private Piece piece;
	private Point position;
	private boolean marked = false;

	public boolean isEmpty() {
		return (getPiece().isEmpty());
	}

	public Square(Color color, int x, int y, Board board) {
		setBoard(board);
		setColor(color);
		setPosition(new Point(x, y));
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		piece.setSquare(this);
		this.piece = piece;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public int differenceInRows(Square square) {
		return square.getPosition().x - this.getPosition().x;
	}

	public int differenceInColumns(Square square) {
		return square.getPosition().y - this.getPosition().y;
	}

	public boolean sameColumns(Square square) {
		return this.differenceInColumns(square) == 0;
	}

	public boolean sameRows(Square square) {
		return this.differenceInRows(square) == 0;
	}

}
