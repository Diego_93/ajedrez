package board;

import game.Game;

import java.awt.Color;
import java.io.Serializable;

import pieces.Bishop;
import pieces.BlackPawn;
import pieces.King;
import pieces.Knight;
import pieces.NoPiece;
import pieces.Queen;
import pieces.Rook;
import pieces.WhitePawn;
import players.Player;

public class Board implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Square[][] squares;
	private Square actualSquare = NoSquare;
	public static final int DIMENSION = 8;
	public static final Square NoSquare = null;
	public static final Player NoPlayer = null;
	private Game game;

	public Board(Game game) {
		
		this.game=game;
		squares = new Square[DIMENSION][DIMENSION];
		
		for (int i = 0; i < DIMENSION; i++) {
			for (int j = 0; j < DIMENSION; j++) {
				if ((i + j) % 2 == 1) {
					squares[i][j] = new Square(Color.GRAY, i, j, this);
				} else {
					squares[i][j] = new Square(Color.WHITE, i, j, this);
				}
			}
		}

	}

	public void fillBoard() {
		
		Player player1= game.getPlayer1();
		Player player2= game.getPlayer2();

		// Reyes
		King king1 = new King();
		King king2 = new King();
		king1.setOwner(player1);
		king2.setOwner(player2);
		getSquares()[7][4].setPiece(king1);
		getSquares()[0][4].setPiece(king2);
		
		// Peones
		for (int i = 0; i < getDimension(); i++) {
			WhitePawn pawn1 = new WhitePawn();
			BlackPawn pawn2 = new BlackPawn();
			pawn1.setOwner(player1);
			pawn2.setOwner(player2);
			getSquares()[6][i].setPiece(pawn1);
			getSquares()[1][i].setPiece(pawn2);
			for (int j = 2; j < getDimension() - 2; j++) {
				getSquares()[j][i].setPiece(new NoPiece());
			}
		}
		// Torres
		Rook rook1 = new Rook();
		Rook rook2 = new Rook();
		Rook rook3 = new Rook();
		Rook rook4 = new Rook();
		rook1.setOwner(player1);
		rook2.setOwner(player1);
		rook3.setOwner(player2);
		rook4.setOwner(player2);
		getSquares()[7][0].setPiece(rook1);
		getSquares()[7][7].setPiece(rook2);
		getSquares()[0][0].setPiece(rook3);
		getSquares()[0][7].setPiece(rook4);
		// Caballos
		Knight knight1 = new Knight();
		Knight knight2 = new Knight();
		Knight knight3 = new Knight();
		Knight knight4 = new Knight();
		knight1.setOwner(player1);
		knight2.setOwner(player1);
		knight3.setOwner(player2);
		knight4.setOwner(player2);
		getSquares()[7][1].setPiece(knight1);
		getSquares()[7][6].setPiece(knight2);
		getSquares()[0][1].setPiece(knight3);
		getSquares()[0][6].setPiece(knight4);
		
		// Alfiles
		Bishop bishop1 = new Bishop();
		Bishop bishop2 = new Bishop();
		Bishop bishop3 = new Bishop();
		Bishop bishop4 = new Bishop();
		bishop1.setOwner(player1);
		bishop2.setOwner(player1);
		bishop3.setOwner(player2);
		bishop4.setOwner(player2);
		getSquares()[7][2].setPiece(bishop1);
		getSquares()[7][5].setPiece(bishop2);
		getSquares()[0][2].setPiece(bishop3);
		getSquares()[0][5].setPiece(bishop4);
		// Reinas
		Queen queen1 = new Queen();
		Queen queen2 = new Queen();
		queen1.setOwner(player1);
		queen2.setOwner(player2);
		getSquares()[7][3].setPiece(queen1);
		getSquares()[0][3].setPiece(queen2);
	}

	public Square[][] getSquares() {
		return squares;
	}

	public void setSquares(Square[][] squares) {
		this.squares = squares;
	}


	public static int getDimension() {
		return DIMENSION;
	}

	public Square getActualSquare() {
		return actualSquare;
	}

	public void setActualSquare(Square actualSquare) {
		this.actualSquare = actualSquare;
	}
	
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	public String code(){
		String aux="";
		for(int i=0; i<DIMENSION; i++) {
			for(int j=0; j<DIMENSION; j++) {
				aux+=squares[i][j].getPiece().code();
			}
		}
		return aux;
	}
	
}
