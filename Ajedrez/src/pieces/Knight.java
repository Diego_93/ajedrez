package pieces;




import board.Square;

public class Knight extends Piece {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Knight";
	}

	@Override
	public boolean validMove(Square destinationSquare)
	{
		if (sameOwner(destinationSquare)) {
			return false;
		}
		if (sameColumns(destinationSquare) || sameRows(destinationSquare)) {
			return false;
		}
		if (Math.abs(differenceInRows(destinationSquare))+Math.abs(differenceInColumns(destinationSquare))==3) {
			return true;
		}
		return false;
	}

	@Override
	public String code() {
		return getOwner().code()+"Kn";
	}
}
