package pieces;

import board.Square;

public class Queen extends Piece {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Queen";
	}

	@Override
	public boolean validMove(Square destinationSquare) {
		if (sameOwner(destinationSquare)) {
			return false;
		}
		if (sameColumns(destinationSquare)	||
			sameRows(destinationSquare)		||
			Math.abs(differenceInColumns(destinationSquare))
			== Math.abs(differenceInRows(destinationSquare))) {
			
			int dx = differenceInRows(destinationSquare);
			if (dx != 0) {
				dx /= Math.abs(dx);
			}
			int dy = differenceInColumns(destinationSquare);
			if (dy != 0) {
				dy /= Math.abs(dy);
			}
			if (checkEmptyPath(destinationSquare, dx, dy)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String code() {
		return getOwner().code()+"Q";
	}
}
