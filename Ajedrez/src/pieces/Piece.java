package pieces;

import java.awt.Point;
import java.io.Serializable;

import players.Player;
import board.Board;
import board.Square;

public abstract class Piece implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Player owner;
	private Square square;
	private boolean moved;

	public abstract String getName();

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
		owner.addPiece(this);
	}

	public boolean isEmpty() {
		return false;
	}

	public Square getSquare() {
		return square;
	}

	public void setSquare(Square square) {
		this.square = square;
	}

	public boolean isMoved() {
		return moved;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	/* Devuelve si puede atacar un casillero en particular */
	public boolean canAttack(Square destinationSquare) {
		return validMove(destinationSquare);
	}

	/*
	 * Devuelve si el movimiento es valido, considerando que el rey no quede
	 * bajo jaque
	 */
	public boolean checkMove(Square destinationSquare) {

		boolean result;
		Square actualSquare = this.getSquare();
		Piece destinationPiece = destinationSquare.getPiece();

		destinationSquare.setPiece(this);
		actualSquare.setPiece(new NoPiece());
		if (!destinationPiece.isEmpty()) {
			destinationPiece.getOwner().removePiece(destinationPiece);
		}
		result = this.getOwner().getKing().isSafe();

		actualSquare.setPiece(this);
		destinationSquare.setPiece(destinationPiece);
		if (!destinationPiece.isEmpty()) {
			destinationPiece.getOwner().addPiece(destinationPiece);
		}

		return result;
	}

	/* Devuelve si puede ejecutar algun movimiento, sin dejar al rey en jaque */
	public boolean canMove() {
		Board board = this.getSquare().getBoard();
		for (int i = 0; i < Board.getDimension(); i++) {
			for (int j = 0; j < Board.getDimension(); j++) {
				Square destinationSquare=board.getSquares()[i][j];
				if (validMove(destinationSquare) && checkMove(destinationSquare)) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * Devuelve si se puede realizar el movimiento (en este caso mueve la pieza)
	 * o no
	 */
	public boolean moveTo(Square destinationSquare) {
		getOwner().setEnPassantColumn(-1); //No se movio un peon 2 casilleros
		if (validMove(destinationSquare)) {
			if (checkMove(destinationSquare)) {
				forcedMove(destinationSquare);
				return true;
			}
			/*
			 * si checkMove=false y validMove=true signifca que al mover quedaba
			 * en jaque
			 */
			print("Movimiento invalido, el rey queda en jaque");
		}
		return false;
	}

	/* Mueve una pieza a un casillero de forma forzada */
	public void forcedMove(Square destinationSquare) {
		Square actualSquare = this.getSquare();
		if (!destinationSquare.isEmpty()) {
			destinationSquare.getPiece().getOwner()
					.removePiece(destinationSquare.getPiece());
		}
		destinationSquare.setPiece(this);
		actualSquare.setPiece(new NoPiece());
		this.setMoved(true);
	}

	abstract public boolean validMove(Square destinationSquare);
	abstract public String code();

	public int differenceInRows(Square square) {
		return this.getSquare().differenceInRows(square);
	}

	public int differenceInColumns(Square square) {
		return this.getSquare().differenceInColumns(square);
	}

	public boolean sameColumns(Square square) {
		return square.sameColumns(this.getSquare());
	}

	public boolean sameRows(Square square) {
		return square.sameRows(this.getSquare());
	}

	public boolean sameOwner(Square square) {
		return getOwner().equals(square.getPiece().getOwner());
	}

	public boolean opponentOwner(Square square) {
		return getOwner().getOpponent().equals(square.getPiece().getOwner());
	}


	/*
	 * Devuelve si el camino desde la pieza hasta la casilla con direccion
	 * (dx,dy) esta vacio
	 */
	public boolean checkEmptyPath(Square destinationSquare, int dx, int dy) {
		Board board = this.getSquare().getBoard();
		Point pointInicial = this.getSquare().getPosition();
		Point pointFinal = new Point(destinationSquare.getPosition().x,
				destinationSquare.getPosition().y);
		for (int i = 1; i * dx + pointInicial.x != pointFinal.x
				|| i * dy + pointInicial.y != pointFinal.y; i++) {
			if (!board.getSquares()[i * dx + pointInicial.x][i * dy
					+ pointInicial.y].isEmpty()) {
				return false;
			}
		}
		return true;
	}

	public void print(String texto) {
		getSquare().getBoard().getGame().print(texto);
	}
}
