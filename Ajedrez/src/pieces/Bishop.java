package pieces;

import board.Square;

public class Bishop extends Piece {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Bishop";
	}

	@Override
	public boolean validMove(Square destinationSquare) {

		if (sameOwner(destinationSquare)) {
			return false;
		}

		if (Math.abs(differenceInRows(destinationSquare)) ==
				Math.abs(differenceInColumns(destinationSquare))) {

			int dx = differenceInRows(destinationSquare);
			if (dx != 0) {
				dx /= Math.abs(dx);
			}
			int dy = differenceInColumns(destinationSquare);
			if (dy != 0) {
				dy /= Math.abs(dy);
			}
			if (checkEmptyPath(destinationSquare, dx, dy)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public String code() {
		return getOwner().code()+"B";
	}
}
