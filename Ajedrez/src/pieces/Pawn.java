package pieces;

public abstract class Pawn extends Piece {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean enPassant;
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Pawn";
	}

	public void promotion() {
		Piece chosenPiece = new Queen();
		this.getSquare().setPiece(chosenPiece);
		chosenPiece.setOwner(this.getOwner());
		this.getOwner().removePiece(this);
	}

	public boolean isEnPassant() {
		return enPassant;
	}

	public void setEnPassant(boolean enPassant) {
		this.enPassant = enPassant;
	}
	
	@Override
	public String code() {
		return getOwner().code()+"P";
	}
	
}
