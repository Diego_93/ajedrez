package pieces;

import board.*;

public class BlackPawn extends Pawn {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public boolean validMove(Square destinationSquare) {
		setEnPassant(false);
		if (sameColumns(destinationSquare)) {
			if (destinationSquare.isEmpty()	&& differenceInRows(destinationSquare) == 1) {
				return true;
			}
			if (!isMoved() && destinationSquare.isEmpty()
					&& differenceInRows(destinationSquare) == 2
					&& checkEmptyPath(destinationSquare, 1, 0)) {
				getOwner().setEnPassantColumn((int) destinationSquare.getPosition().getY());
				return true;
			}
		}
		
		return canAttack(destinationSquare);
	}

	public boolean moveTo(Square destinationSquare) {
		if (!super.moveTo(destinationSquare)) {
			return false;
		}

		if (isEnPassant()) {
			Board board = getSquare().getBoard();
			Square removePieceSquare =	board.getSquares()
					[(int) destinationSquare.getPosition().getX() - 1]
					[(int) destinationSquare.getPosition().getY()];
			removePieceSquare.getPiece().getOwner()
					.removePiece(removePieceSquare.getPiece());
			removePieceSquare.setPiece(new NoPiece());
		}

		if (destinationSquare.getPosition().x == 7) {
			this.promotion();
		}
		getSquare().getBoard().getGame().moveAPawn();
		return true;
	}

	public boolean canAttack(Square destinationSquare) {
		if (Math.abs(differenceInColumns(destinationSquare)) == 1
				&& differenceInRows(destinationSquare) == 1) {
			if (opponentOwner(destinationSquare)) {
				return true;
			}
			
			/*Verifica si puede comer el peon al paso*/
			if (enPassantSquare(destinationSquare)) {
				setEnPassant(true);
				return true;
			}
		}
		return false;
	}

	public boolean enPassantSquare(Square destinationSquare) {
		return destinationSquare.getPosition().getY() == getOwner()
				.getOpponent().getEnPassantColumn()
				&& destinationSquare.getPosition().getX() == 5;
	}


}
