package pieces;

import players.Player;
import board.Board;
import board.Square;


public class King extends Piece {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean shortCastling;
	private boolean longCastling;
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "King";
	}

	public void setOwner(Player owner) {
		super.setOwner(owner);
		owner.setKing(this);
	}

	public boolean isSafe() {
		return !(this.getOwner().getOpponent().canAttack(this.getSquare()));
	}

	@Override
	public boolean validMove(Square destinationSquare) {
		setShortCastling(false);
		setLongCastling(false);
		if (canAttack(destinationSquare)) {
			return true;
		}
		return canDoCastling(destinationSquare);
	}

	public boolean canAttack(Square destinationSquare) {
		if (sameOwner(destinationSquare))
			return false;

		if (Math.abs(differenceInRows(destinationSquare)) < 2
				&& Math.abs(differenceInColumns(destinationSquare)) < 2)
			return true;
		return false;
	}

	public boolean canDoCastling(Square destinationSquare)
	{
		Board board = getSquare().getBoard();
		Square actualSquare = getSquare();
		boolean result = false;
		// Posible enroque
		if (!isMoved()	&& sameRows(destinationSquare)) {
			// Enroque largo
			if (destinationSquare.getPosition().y == 2
					&& !board.getSquares()[actualSquare.getPosition().x][0].isEmpty()
					&& !board.getSquares()[actualSquare.getPosition().x][0].getPiece().isMoved()) {
				// Fila vacia
				if (checkEmptyPath(board.getSquares()[actualSquare.getPosition().x][0], 0,-1)) {
					if (this.isSafe()) {
						this.getSquare().setPiece(new NoPiece());
						board.getSquares()[actualSquare.getPosition().x][3].setPiece(this);
						if (this.isSafe()) {
							this.getSquare().setPiece(new NoPiece());
							board.getSquares()[actualSquare.getPosition().x][2].setPiece(this);
							if (this.isSafe()) {
								setLongCastling(true);
								result = true;
							}
							this.getSquare().setPiece(new NoPiece());
						}
						this.getSquare().setPiece(new NoPiece());
						board.getSquares()[actualSquare.getPosition().x][4].setPiece(this);
					}
				}
			}
	
			// Enroque corto
			if (destinationSquare.getPosition().y == 6
					&& !board.getSquares()[actualSquare.getPosition().x][7].isEmpty()
					&& !board.getSquares()[actualSquare.getPosition().x][7].getPiece().isMoved()) {
				// Fila vacia
				if (checkEmptyPath(board.getSquares()[actualSquare.getPosition().x][7], 0,	1)) {
					if (this.isSafe()) {
						this.getSquare().setPiece(new NoPiece());
						board.getSquares()[actualSquare.getPosition().x][5]
								.setPiece(this);
						if (this.isSafe()) {
							this.getSquare().setPiece(new NoPiece());
							board.getSquares()[actualSquare.getPosition().x][6]
									.setPiece(this);
							if (this.isSafe()) {
								setShortCastling(true);
								result = true;
							}
							this.getSquare().setPiece(new NoPiece());
						}
						this.getSquare().setPiece(new NoPiece());
						board.getSquares()[actualSquare.getPosition().x][4].setPiece(this);
					}
	
				}
			}
		}
		return result;
	}
	
	public boolean moveTo(Square destinationSquare)
	{
		if (!super.moveTo(destinationSquare))
		{
			return false;
		}
		Square actualSquare = getSquare();
		Board board= actualSquare.getBoard();
		if (isLongCastling()) {
			board.getSquares()[actualSquare.getPosition().x][0].getPiece().forcedMove(
					board.getSquares()[actualSquare.getPosition().x][3]);
		}
		if (isShortCastling()) {
			board.getSquares()[actualSquare.getPosition().x][7].getPiece().forcedMove(
					board.getSquares()[actualSquare.getPosition().x][5]);
		}
		return true;
	}

	public boolean isShortCastling() {
		return shortCastling;
	}

	public void setShortCastling(boolean shortCastling) {
		this.shortCastling = shortCastling;
	}

	public boolean isLongCastling() {
		return longCastling;
	}

	public void setLongCastling(boolean longCastling) {
		this.longCastling = longCastling;
	}

	@Override
	public String code() {
		return getOwner().code()+"Ki";
	}
	
	
}
