package pieces;

import board.Square;

public class NoPiece extends Piece {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "NoPiece";
	}

	public boolean isEmpty()
	{
		return true;
	}

	@Override
	public boolean validMove(Square destinationSquare) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public String code() {
		return "-";
	}
	
}
